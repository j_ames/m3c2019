"""MATH96012 2019 Project 1
Add your name and college id here
"""
import numpy as np
import matplotlib.pyplot as plt
#--------------------------------

def simulate1(N=64,L=8,s0=0.2,r0=1,A=0.2,Nt=100):
    """Part1: Simulate bacterial colony dynamics
    Input:
    N: number of particles
    L: length of side of square domain
    s0: speed of particles
    r0: particles within distance r0 of particle i influence direction of motion
    of particle i
    A: amplitude of noise
    dt: time step
    Nt: number of time steps

    Output:
    X,Y: position of all N particles at Nt+1 times
    alpha: alignment parameter at Nt+1 times

    Do not modify input or return statement without instructor's permission.

    Add brief description of approach to problem here:
    """
    #Set initial condition
    phi_init = np.random.rand(N)*(2*np.pi)
    r_init = np.sqrt(np.random.rand(N))
    Xinit,Yinit = r_init*np.cos(phi_init),r_init*np.sin(phi_init)
    Xinit+=L/2
    Yinit+=L/2

    theta_init = np.random.rand(N)*(2*np.pi) #initial directions of motion
    #---------------------

    return X,Y,alpha


def analyze():
    """Part 2: Add input variables and modify return as needed
    """
    return None



def simulate2():
    """Part 2: Simulation code for Part 2, add input variables and simulation
    code needed for simulations required by analyze

    """


#--------------------------------------------------------------
if __name__ == '__main__':
    #The code here should call analyze and
    #generate the figures that you are submitting with your
    #discussion.

    output_a = analyze()
